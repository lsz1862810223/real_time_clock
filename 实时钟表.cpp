#include <graphics.h>	 // 引用 EasyX 图形库
#include <conio.h>
#include<math.h>
#define Π 3.1415926
#define high 480
#define width 640
void main()
{
	int hour, minute, second;
	initgraph(width, high);	// 初始化640×480的画布
	int center_x = width / 2;//画布中心x坐标
	int center_y = high / 2;//画布中心y坐标
	int secondlength;//秒针长度
	int minutelength;//分针长度
	int hourlength;//时针长度
	secondlength = width / 5;
	minutelength = width / 7;
	hourlength = width / 9;
	int secondend_x, secondend_y;//秒针端点的位置
	int minuteend_x, minuteend_y;//分针端点的位置
	int hourend_x, hourend_y;//时针端点的位置
	float secondangle = 0;//秒针转动的角度
	float minuteangle = 0;//分针转动的角度
	float hourangle = 0; //时针转动的角度
	SYSTEMTIME ti;//定义变量存储系统时间
	BeginBatchDraw();
	while (1)
	{
		GetLocalTime(&ti);//获取当前时间
		secondangle = ti.wSecond * 2 * Π / 60;//秒针转动的角度
		secondend_x = center_x + secondlength * sin(secondangle);//秒针端点的位置
		secondend_y = center_y - secondlength * cos(secondangle);
		minuteangle = ti.wMinute * 2 * Π / 60;//分针转动的角度
		minuteend_x = center_x + minutelength * sin(minuteangle);//分针端点位置
		minuteend_y = center_y - minutelength * cos(minuteangle);
		hourangle = ti.wHour * 2 * Π / 12;//分时转动的角度
		hourend_x = center_x + hourlength * sin(hourangle);//时针端点位置
		hourend_y = center_y - hourlength * cos(hourangle);
		setcolor(BLUE);//画圆
		circle(320, 240, 200);
		setlinestyle(PS_SOLID);//画秒针
		setcolor(WHITE);
		line(center_x, center_y, secondend_x, secondend_y);
		setlinestyle(PS_SOLID,3);//画分针
		setcolor(YELLOW);
		line(center_x, center_y, minuteend_x, minuteend_y);
		setlinestyle(PS_SOLID,5);//画时针
		setcolor(LIGHTRED);
		line(center_x, center_y, hourend_x, hourend_y);
		FlushBatchDraw();
		cleardevice();
	}
    EndBatchDraw();
	getch();//按任意键结束进程
	closegraph();
}